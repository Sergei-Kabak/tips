# Tips

<details>
<summary> 

## Install pod 
</summary> 

For Homebrew:

Вставить в терминале :

```brew install ruby```


Командой ```brew info ruby``` узнать путь и добавить ссылку в файле ```/etc/zshrc``` :

```export PATH=/opt/homebrew/lib/ruby/gems/3.1.0/bin:$PATH``` 

Командой узнать путь к ```brew```

```$(brew --prefix)```

Принять настройки 

```source /etc/zshrc``` 

Проверим путь

```which ruby```

Должно быть так :

```$(brew --prefix)/opt/ruby/bin/ruby```

</details>


<details>
<summary> 

## Install CocoaPods 
</summary>

```sudo gem install cocoapods```


Проверяем путь : 

```which pod``` 

Корректный путь :

```$(brew --prefix)/lib/ruby/gems/3.0.0/bin/pod```


</details>


<details>
<summary> 

## Установим ethon
</summary>

```sudo gem install ethon```

Run --->```gem info ethon```


Run --->```pod install```

</details>

<details>
<summary> 

## Install Java 11
</summary> 

```brew install java11```

Add link

```sudo ln -sfn /usr/local/opt/openjdk@11/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk-11.jdk```

Check version

```java --version```

</details>

<details>
<summary> 

## zshrc
</summary> 

```
export FLUTTER_ROOT="/Users/rayan/development/SDK/flutter"
export PATH="/Users/rayan/development/SDK/cmdline-tools/latest/bin:$PATH"
export PATH="/opt/homebrew/opt/ruby/bin:$PATH"
export PATH="$PATH:/Users/rayan/development/SDK/flutter/bin"
export LDFLAGS="-L/opt/homebrew/opt/ruby/lib"
export CPPFLAGS="-I/opt/homebrew/opt/ruby/include"
export PATH="/opt/homebrew/Cellar/cocoapods/1.11.3/bin/pod:$PATH"
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export PATH="/Users/rayan/development/SDK/platform-tools:$PATH"
export PATH="/Users/rayan/development/SDK/emulator:$PATH"
export CPPFLAGS="-I/opt/homebrew/opt/openjdk@11/include"
export PATH="/opt/homebrew/opt/openjdk@11/bin:$PATH"

alias pod='/opt/homebrew/Cellar/cocoapods/1.11.3/bin/pod'
alias cmd='source /etc/zshrc'
alias cls='clear'
alias watchd='flutter pub run build_runner watch --delete-conflicting-outputs'
alias buildd='flutter pub run build_runner build --delete-conflicting-outputs'
alias edt='sudo nano /etc/zshrc'
alias pbrn='flutter pub run'
alias cln='flutter clean'
alias pbgt='flutter pub get' 
```

</details>

<details>
<summary> 

## WIFI connect 
</summary> 


```adb pair 192.168.100.107:39659```

enter code 

```adb connect 192.168.100.107:37869```

</details>

<details>
<summary> 

## Localization
</summary>

translation.dart

```
extension Localization on BuildContext {
  String text(String key) {
    return Translations.of(this)._getTranslate(key) ?? "not found";
  }
}
```
```
class TranslationsDelegate extends LocalizationsDelegate<Translations> {
  const TranslationsDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<Translations> load(Locale locale) => Translations.load(locale);

  @override
  bool shouldReload(TranslationsDelegate old) => false;
}
```
```
class Translations {
  static const List<String> _supportedLocales = [
    'en',
    'ru',
  ];

  static late Map<String, dynamic> _mapKeyValueTranslate;

  Translations(Locale locale) : super();

  String? _getTranslate(String? key) => _mapKeyValueTranslate[key];

  static Translations of(BuildContext context) {
    return Localizations.of<Translations>(context, Translations) ??
        Translations(const Locale("ru"));
  }

  static Future<Translations> load(Locale locale) async {
    if (!_supportedLocales.contains(locale.languageCode)) {
      locale = const Locale("ru");
    }

    String jsonContent =
        await rootBundle.loadString("locale/i18n_${locale.languageCode}.json");
    _mapKeyValueTranslate = json.decode(jsonContent);

    Translations translations = Translations(locale);

    return translations;
  }
}
```

app.dart

``` 
localizationsDelegates: const [
                    TranslationsDelegate(),
                    GlobalCupertinoLocalizations.delegate,
                    GlobalMaterialLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate,
                  ],
supportedLocales: const [
                    Locale('en'),
                    Locale('ru'),
                  ],
```

Folders :
``` 
locale/i18n_en.json 
locale/i18n_ru.json
```
</details>
